﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public float speed;
    Rigidbody rb;
    Animator anim;
    float timer;
    float vertAxis;
    float horizAxis;
    Camera cam;
    public Transform cameraHolder;
    public Transform targetCam;
    public Transform rainHolder;
    public Transform targetRain;
    Vector3 rotCam;
    public float range = 100f;
    bool isAiming, isTarget;
    public int gunDamage;
    public int ammo;
    public int health;
    public Transform shootParticlePoint;
    public ParticleSystem shootParticle;
    public ParticleSystem hitShootParticle;
    public bool Scream;
    public float ScreamDelay = 3;
    float ScreamTimer = 0;

    public GameObject AbilityStop;
    public GameObject player;
    public GameObject playerRagdoll;
    public GameObject HUD;

    public AudioSource audioSource;
    public AudioSource audioSource2;
    public AudioSource audioSourceDeath;
    public AudioClip[] audioClips;

    public GameObject pauseMenu;
    public GameObject gameOverMenu;
    public GameObject finishMenu;

    void Start()
    {
        //Time.timeScale = 1f;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        timer = 0;
        ammo = 20;
        health = 100;
        cam = Camera.main;
        isAiming = false;
        isTarget = false;
        Scream = false;
    }

    void Update()
    {
        cameraHolder.position = targetCam.position;
        rainHolder.position = targetRain.position;
        timer += Time.deltaTime;
        ScreamTimer += Time.deltaTime;
        anim.SetFloat("speed", vertAxis);
        //if(!Input.anyKey && Input.GetAxis("Mouse X") == 0 && Input.GetAxis("Mouse Y") == 0)
        //{
        //    if(timer>4)
        //    {
        //        anim.SetBool("idleLook", true);
        //    }
        //}
        //else
        //{
        //    timer = 0;
        //    anim.SetBool("idleLook", false);
        //}
        if (Input.GetKeyDown(KeyCode.C))
        {
            Cursor.visible = !Cursor.visible;
            Cursor.lockState = Cursor.visible ? CursorLockMode.None : CursorLockMode.Locked;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            Arrest();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 0f;
            pauseMenu.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            health = 100;
        }

            if (Input.GetMouseButton(1))
        {
            isAiming = true;
            anim.SetBool("isAiming", true);

            if (ammo != 0)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    Shoot();
                }
            }
        }
        else
        {
            isAiming = false;
            anim.SetBool("isAiming", false);
        }

        //Debug.Log("ammo = " + ammo);

        if (health <= 0)
        {
            Dead();
        }
    }

    private void FixedUpdate()
    {
        vertAxis = Input.GetAxis("Vertical");
        horizAxis = Input.GetAxis("Horizontal");
        if (vertAxis != 0)
        {
            rb.velocity = transform.TransformDirection(new Vector3(horizAxis * speed * Time.deltaTime, rb.velocity.y, vertAxis * speed * Time.deltaTime));
            AudioWalk();
        }
        if (horizAxis < 0)
        {
            anim.SetBool("isStrafeLeft", true);
            anim.SetBool("isStrafeRight", false);
            AudioWalk();
        }
        else if (horizAxis > 0)
        {
            anim.SetBool("isStrafeLeft", false);
            anim.SetBool("isStrafeRight", true);
            AudioWalk();
        }
        else
        {
            anim.SetBool("isStrafeLeft", false);
            anim.SetBool("isStrafeRight", false);
        }

        if (isAiming == false)
        {
            isTarget = false;
            transform.Rotate(0, horizAxis * speed * Time.deltaTime, 0, Space.World);
            rotCam = cameraHolder.transform.localEulerAngles;
            cameraHolder.transform.Rotate(-Input.GetAxis("Mouse Y") * 2f, 0, 0, Space.Self);
            cameraHolder.transform.Rotate(0, Input.GetAxis("Mouse X") * speed * Time.deltaTime, 0, Space.World);
            if ((cameraHolder.transform.localEulerAngles.x > 310 && cameraHolder.transform.localEulerAngles.x < 360) || (cameraHolder.transform.localEulerAngles.x > 0 && cameraHolder.transform.localEulerAngles.x < 50))
            {
            }
            else
            {
                cameraHolder.transform.localEulerAngles = rotCam;
            }
        }
        else
        {
            if (!isTarget)
            {
                isTarget = true;
                StartCoroutine(CameraAimingLerp());
            }
            rotCam = cameraHolder.transform.localEulerAngles;
            cameraHolder.transform.Rotate(-Input.GetAxis("Mouse Y") * 2f, 0, 0, Space.Self);
            cameraHolder.transform.Rotate(0, Input.GetAxis("Mouse X") * speed * Time.deltaTime, 0, Space.World);
            transform.Rotate(0, Input.GetAxis("Mouse X") * speed * Time.deltaTime, 0, Space.World);
            if ((cameraHolder.transform.localEulerAngles.x > 310 && cameraHolder.transform.localEulerAngles.x < 360) || (cameraHolder.transform.localEulerAngles.x > 0 && cameraHolder.transform.localEulerAngles.x < 50))
            {
            }
            else
            {
                cameraHolder.transform.localEulerAngles = rotCam;
            }
        }
        if (ScreamTimer > ScreamDelay)
        {
            AbilityStop.SetActive(false);
            if (Input.GetKeyDown(KeyCode.Q))
            {
                Scream = true;
                ScreamTimer = 0;
                audioSource.clip = audioClips[5];
                audioSource.Play();
            }
     
            
        }
        else
        {
            Scream = false;
            AbilityStop.SetActive(true);
        }
    }

    void Arrest()
    {
        RaycastHit hit;
        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, range))
        {
            if (hit.collider.GetComponent<NavEnemyController>())
            {
                if (hit.collider.GetComponent<NavEnemyController>().arrested)
                {
                    Destroy(hit.collider.GetComponent<NavEnemyController>().transform.gameObject,1f);
                }
                
            }
        }
    }

    void Shoot()
    {
        if (timer > 0.2f)
        {
            RaycastHit hit;
            if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, range))
            {
                Instantiate(hitShootParticle, hit.point, Quaternion.identity);


                //Debug.Log(hit.transform.name);
                gunDamage = Random.Range(10, 100);
                if (hit.collider.GetComponent<EnemyController>())
                {
                    hit.collider.GetComponent<EnemyController>().EnemyDamage(gunDamage);
                    Debug.Log("gunDamage" + gunDamage);
                }
                if (hit.collider.GetComponent<BurrelExplosion>())
                {
                    hit.collider.GetComponent<BurrelExplosion>().Explode();
                }
                if (hit.collider.GetComponent<ButtonController>())
                {
                    hit.collider.GetComponent<ButtonController>().ActiveButton();
                }
            }
            Instantiate(shootParticle, shootParticlePoint);
            audioSource.clip = audioClips[1];
            audioSource.Play();
            timer = 0;
            ammo--;
        }
    }

    IEnumerator CameraAimingLerp()
    {
        while (true)
        {
            yield return null;
            cameraHolder.eulerAngles = Vector3.MoveTowards(cameraHolder.eulerAngles, new Vector3(cameraHolder.eulerAngles.x, transform.eulerAngles.y, 0), 20f);
            if (Vector3.Distance(cameraHolder.eulerAngles, new Vector3(cameraHolder.eulerAngles.x, transform.eulerAngles.y, 0)) < 0.01f)
            {
                yield break;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ammo")
        {
            ammo = 20;
            audioSource.clip = audioClips[4];
            audioSource.Play();
            Destroy(other.gameObject);
        }
        if (other.gameObject.tag == "Finish")
        {
            Time.timeScale = 0f;
            finishMenu.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void Damage(int damage)
    {
        audioSource.clip = audioClips[2];
        audioSource.Play();

        health -= damage;
        //Debug.Log("Health = " + health);
    }

    public void Dead()
    {
        audioSourceDeath.Play();

        player.SetActive(false);
        Instantiate(playerRagdoll, transform.position, transform.rotation);
        HUD.SetActive(false);
        Destroy(gameObject, 1f);

        gameOverMenu.SetActive(true);

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void AudioWalk()
    {
        if (!audioSource2.isPlaying)
        {
            audioSource2.clip = audioClips[0];
            audioSource2.Play();
            if (vertAxis > 0)
            {
                audioSource2.pitch = 1.7f;
            }
            if (vertAxis < 0)
            {
                audioSource2.pitch = 1.3f;
            }

            if (vertAxis == 0 && horizAxis == 0)
            {
                audioSource2.Stop();
            }

            if (horizAxis < 0 || horizAxis < 0)
            {
                audioSource2.pitch = 1.2f;
            }
        }
    }
}
