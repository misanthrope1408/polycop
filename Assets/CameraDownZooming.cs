﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDownZooming : MonoBehaviour
{
    public Transform target;
    //public float speedX = 360f;
    //public float speedY = 180f;
    //public float limit = 40f;
    public float hideDis = 2f;
    public LayerMask obstacle;
    public LayerMask noPlayer;
    float maxDis;
    //float currentYRot;
    LayerMask camStart;
    Camera cam;

    void Start()
    {
        cam = GetComponent<Camera>();
        maxDis = Vector3.Distance(transform.position, target.position);
        camStart = cam.cullingMask;
        transform.LookAt(target);
        //currentYRot = 0;
    }

    void Update()
    {
        //transform.parent.Rotate(0, Input.GetAxis("Mouse X") * speedX * Time.deltaTime, 0, Space.World);

        //float mouseY = -Input.GetAxis("Mouse Y");
        //if (mouseY != 0)
        //{
        //    float temp = Mathf.Clamp(currentYRot + mouseY * speedY * Time.deltaTime, -limit, limit);
        //    if (temp != currentYRot)
        //    {
        //        transform.RotateAround(target.position, transform.right, temp - currentYRot);
        //        currentYRot = temp;
        //    }
        //}

        float distance = Vector3.Distance(transform.position, target.position);
        RaycastHit hit;
        if (Physics.Raycast(target.position, transform.position - target.position, out hit, maxDis, obstacle))
        {
            transform.position = hit.point;
        }
        else if (distance < maxDis && !Physics.Raycast(transform.position, -transform.forward, 0.1f, obstacle))
        {
            transform.position -= transform.forward * 0.05f;
        }

        if (distance < hideDis)
        {
            cam.cullingMask = noPlayer;
        }
        else
        {
            cam.cullingMask = camStart;
        }

    }
}

