﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSController : MonoBehaviour {

    public float speed;
    Rigidbody rb;
    Animator anim;
    float timer;

    float vertAxis;

    Camera cam;
    public Transform cameraHolder;
    public Transform chest;

    Vector3 rotCam;

    public float range = 100f;
    float flowRotate;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        timer = 0;

        cam = Camera.main;
    }

    void Update()
    {
        timer += Time.deltaTime;

        anim.SetFloat("speed", vertAxis);

        if (vertAxis == 0)
        {
            flowRotate += Input.GetAxis("Mouse X") / 20;
            anim.SetFloat("turn", flowRotate);
            //anim.SetFloat("turn", Input.GetAxis("Mouse X") * speed * Time.deltaTime);
        }
        else
        {
            flowRotate = 0;

        }
        if (!Input.anyKey && Input.GetAxis("Mouse X") == 0 && Input.GetAxis("Mouse Y") == 0)
        {
            if (timer > 4)
            {
                anim.SetBool("idleLook", true);
            }
        }
        else
        {
            timer = 0;
            anim.SetBool("idleLook", false);
        }

        if (Input.GetButtonDown("Fire1"))
        {
           // Shoot();
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            Cursor.visible = !Cursor.visible;
            Cursor.lockState = Cursor.visible ? CursorLockMode.None : CursorLockMode.Locked;
        }

    }

    private void FixedUpdate()
    {
        vertAxis = Input.GetAxis("Vertical");

        rb.velocity = transform.TransformDirection(new Vector3(0, rb.velocity.y, vertAxis * speed * Time.deltaTime));
        if (vertAxis != 0)
        {
            transform.Rotate(0, Input.GetAxis("Mouse X") * speed * Time.deltaTime, 0);
        }

        rotCam = cameraHolder.transform.localEulerAngles;
        cameraHolder.transform.Rotate(-Input.GetAxis("Mouse Y") * 2f, 0, 0, Space.Self);
        if ((cameraHolder.transform.localEulerAngles.x > 310 && cameraHolder.transform.localEulerAngles.x < 360) || (cameraHolder.transform.localEulerAngles.x > 0 && cameraHolder.transform.localEulerAngles.x < 50))
        {
        }
        else
        {
            cameraHolder.transform.localEulerAngles = rotCam;
        }


    }

    void Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);
        }
    }
}
