﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGUI : MonoBehaviour {

    public Texture crosshair;

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(Screen.width/2 - crosshair.width/2, Screen.height / 2 - crosshair.height / 2, crosshair.width, crosshair.height), crosshair);
    }
}
