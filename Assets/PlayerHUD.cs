﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHUD : MonoBehaviour {

    public PlayerController playerController;
    public Image healthbarUp;
    public Text ammoText;

	void Update () 
    {
        ammoText.text = playerController.ammo.ToString();
        healthbarUp.fillAmount = playerController.health / 100f;

    }
}
