﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurrelExplosion : MonoBehaviour {

    public GameObject explosionEffect;
    public float radius = 10f;
    public float explosionForce = 700f;
    bool isExplosion = false;

    public AudioSource audioSource;
    public AudioClip audioClip;

    MeshRenderer meshRenderer;

    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    public void Explode()
    {
        Instantiate(explosionEffect, transform.position, transform.rotation);
        meshRenderer.enabled = false;

        audioSource.PlayOneShot(audioClip);

        if (!isExplosion)
        {
            isExplosion = true;
            Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
            foreach (Collider nearbyObjects in colliders)
            {
                Debug.Log("GUYS");
                Rigidbody rb = nearbyObjects.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    if (nearbyObjects.gameObject.GetComponent<BurrelExplosion>())
                    {
                        nearbyObjects.gameObject.GetComponent<BurrelExplosion>().Explode();
                    }
                    if (nearbyObjects.gameObject.GetComponentInChildren<PlayerController>())
                    {
                        nearbyObjects.gameObject.GetComponentInChildren<PlayerController>().Dead();
                    }
                    Debug.Log("player");
                    rb.AddExplosionForce(explosionForce, transform.position, radius);
                }
                else if (nearbyObjects.gameObject.GetComponent<EnemyController>())
                {
                    Rigidbody enemyRb = nearbyObjects.gameObject.AddComponent<Rigidbody>();
                    nearbyObjects.gameObject.GetComponent<EnemyController>().EnemyDead();
                    enemyRb.AddExplosionForce(explosionForce, transform.position, radius);
                }
            }
        }
        Destroy(gameObject, 2);
    }
}
