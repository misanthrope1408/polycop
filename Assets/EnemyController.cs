﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public float timer;
    public float range;
    public int enemyAmmo;
    public int enemyHealth;
    public int gunDamage;
    public int bullTimer;
    public int bullRandEq = 1;
    public int rayTimer;
    public int randEq = 1;
    public Transform shootParticlePoint;
    public ParticleSystem shootParticle;
    public ParticleSystem hitShootParticle;

    public GameObject enemyModel;
    public GameObject enemyRagdoll;

    public AudioSource audioSource;
    public AudioClip audioClip;

    void Start () {
        enemyAmmo = 100;
        enemyHealth = 100;
        range = 100f;
    }
	
	void Update () {
        timer += Time.deltaTime;
        if (timer > 0.5f)
        {
            EnemyShoot();
        }

        if (enemyHealth <= 0)
        {
            EnemyDead();
        }
    }

    void EnemyShoot()
    {
        bullTimer = Random.Range(1, 3);
        if (bullTimer == bullRandEq)
        {
            rayTimer = Random.Range(1, 3);
            if (rayTimer == randEq)
            {
                //Debug.Log("shoot");
                RaycastHit hit;
                Debug.DrawRay(shootParticlePoint.position, shootParticlePoint.forward*20f, Color.red, 2f);
                if (Physics.Raycast(shootParticlePoint.position, shootParticlePoint.forward, out hit, range))
                {
                    //Debug.Log(hit.transform.name);
                    Instantiate(hitShootParticle, hit.point, Quaternion.identity);

                    gunDamage = Random.Range(5, 20);
                    if (hit.collider.GetComponent<PlayerController>())
                    {
                        hit.collider.GetComponent<PlayerController>().Damage(gunDamage);
                    }
                }
            }
            audioSource.clip = audioClip;
            audioSource.Play();
            Instantiate(shootParticle, shootParticlePoint);
            timer = 0;
            enemyAmmo--;
            
            //Debug.Log("timer = " + timer + "enemyAmmo = " + enemyAmmo + "rayTimer = " + rayTimer);
        }
        timer = 0;
        //Debug.Log("bulltimer = " + bullTimer);
     }

    public void EnemyDamage(int damage)
    {
        enemyHealth -= damage;
        //Debug.Log("enemyHealth" + enemyHealth);
    }

    public void EnemyDead()
    {
        Destroy(gameObject);
        Instantiate(enemyRagdoll, transform.position, transform.rotation);
    }

    private void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "Container")
        {
            EnemyDead();
        }
    }
}
