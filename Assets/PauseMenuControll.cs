﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuControll : MonoBehaviour {

    public GameObject pauseMenu;
    public GameObject gameOverMenu;

    public void ResumeButton()
    {
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);
    }

    public void RestartButton()
    {
        Time.timeScale = 1f;
        gameOverMenu.SetActive(false);
        SceneManager.LoadScene(1);
    }

    public void MainMenuButton()
    {
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);
        SceneManager.LoadScene(0);
    }
}
