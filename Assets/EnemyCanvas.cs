﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EnemyCanvas : MonoBehaviour {

    Camera cam;
    public EnemyController enemyController;
    public Image enemyHealthbarUp;

    void Start () 
    {
        cam = Camera.main;
	}
	
	void Update () 
    {
        transform.LookAt(cam.transform.position);
        enemyHealthbarUp.fillAmount = enemyController.enemyHealth / 100f;
    }
}
