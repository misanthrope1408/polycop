﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour {

    public GameObject lightObj;
    public GameObject container;

    public AudioSource audioSourceDeath;

    public void ActiveButton()
    {
        lightObj.SetActive(false);
        container.GetComponent<Rigidbody>().isKinematic = false;
        audioSourceDeath.Play();
    }
}
