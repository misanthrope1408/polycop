﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterKiller : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerController>())
        {
            other.GetComponent<PlayerController>().Dead();
        }
        else
        {
            Destroy(other.GetComponent<Transform>().gameObject);
        }
    }
}
