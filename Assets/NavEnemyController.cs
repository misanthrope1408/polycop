﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
//using UnityStandardAssets.Characters.ThirdPerson;


public class NavEnemyController : MonoBehaviour
{
    public Transform sens;
    public Transform markermap1;
    public Transform markermap2;
    public float LookRadius = 10f;
    public Transform target;
    public float AttackDistanceDivider = 1.3f;
    public float WaitTimer = 5f;
    bool VisitedPosition = true;
    public float RaycastProbeDistance = 100f;
    public float DeadZone = 5f;
    int FearLevel;
    public int FearLevelEq = 1;
    public LayerMask EnemyLayerMask;
    public bool CallMyFriends = false; //call backup if player noticed
    public bool RogerThat = false; //If enemy accept backup call, not need to call again -recursive protection?
    public bool arrested = false;
    public Transform Gun;
    private List<Vector3> PathTemp = new List<Vector3>();

    public GameObject enemyHealthbar;
    public GameObject surrendImage;

    NavMeshAgent agent;
    Animator anim;

    Vector3 lastSeenPoint;
    //public ThirdPersonCharacter character;
    float timer;
    float timer2;
    bool dontAllowRandom = false;
    void Start()
    {

        timer = 0;
        timer2 = 0;
    }
    private void Awake()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        agent.avoidancePriority = Random.Range(0,100);
        agent.updateRotation = false;
        findTarget();
    }
    void findTarget()
    {
        Vector3 destination = new Vector3(Random.Range(markermap1.position.x, markermap2.position.x), 5f, Random.Range(markermap1.position.z, markermap2.position.z));
        CheckPointBeforeVisit(destination);
        print("Random point is=" +destination);
        allowTrigger = true;
        
        Debug.DrawRay(transform.position, destination, Color.green, 2, false);

        print("Go to random point");
    }
    void FaceTraget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRottation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRottation, Time.deltaTime * 5f);

        Gun.rotation = Quaternion.Slerp(transform.rotation, lookRottation, Time.deltaTime);
        //Debug.DrawRay(transform.position, direction * 15, Color.blue, 0.5f);

    }
    void Attack(bool state)
    {
        if (state)
        {

            Vector3 direction = (target.position - transform.position).normalized;
            RaycastHit hit;
            Debug.DrawRay(transform.position, direction * 20, Color.blue, 0.5f);
            if (Physics.Raycast(transform.position, direction * 20, out hit, RaycastProbeDistance))
            {
                //Debug.Log(hit.transform.name);
                if (hit.transform.name == target.name)
                {
                    transform.GetComponent<EnemyController>().enabled = true;
                }
                else
                {
                    transform.GetComponent<EnemyController>().enabled = false;
                }
            }

        }
        else
        {
            transform.GetComponent<EnemyController>().enabled = false;
        }

    }
    bool CheckDistanceAmongPlayerEnemy()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance <= LookRadius)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool allowTrigger = true;
    void CheckPointBeforeVisit(Vector3 point, bool ForceStop = false, bool StopByDistance = false)
    {
        if (ForceStop)
        {
            agent.isStopped = true;
            allowTrigger = true;
        }

        //get point and move to, don't move to new, before visited this 
        if (allowTrigger)
        {
            agent.isStopped = false;
            agent.SetDestination(point);
            allowTrigger = false;
        }
        if (!allowTrigger)
        {
            if (agent.remainingDistance < agent.stoppingDistance)
            {
                print("StopAgent" + agent.remainingDistance);
                //agent.Stop();
                //agent.SetDestination(transform.position);
                allowTrigger = true;
            }
        }
    }
    void Fear()
    {

        if (target.GetComponent<PlayerController>().Scream)
        {

            FearLevel = Random.Range(1, FearLevelEq);
            if (CheckDistanceAmongPlayerEnemy())
            {


                if (FearLevel == FearLevelEq-1)
                {
                    agent.isStopped = true;

                    target = null;
                    foreach (Renderer child in GetComponentsInChildren<Renderer>())
                    {
                        child.material.color = Color.blue;
                    }
                    
                    agent.enabled = false;

                    enemyHealthbar.SetActive(false);
                    surrendImage.SetActive(true);
                    arrested = true;
                    anim.SetBool("Waving", true);
                }
            }
        }

    }
    private void FixedUpdate()
    {
        if (target)
        {
            Fear();


            float vertAxis = agent.velocity.magnitude;
            float horizAxis = agent.velocity.z;

            if (horizAxis < 0)
            {
                anim.SetBool("isStrafeLeft", true);
                anim.SetBool("isStrafeRight", false);
            }
            else if (horizAxis > 0)
            {
                anim.SetBool("isStrafeLeft", false);
                anim.SetBool("isStrafeRight", true);
            }
            else
            {
                anim.SetBool("isStrafeLeft", false);
                anim.SetBool("isStrafeRight", false);
            }

            anim.SetFloat("enemySpeed", horizAxis);

        }

    }
    void CallBackup()
    {
        Collider[] EnemyFriendsColliders = Physics.OverlapSphere(transform.position, LookRadius * 2, EnemyLayerMask);

        foreach (Collider friend in EnemyFriendsColliders)
        {
            print("Call Backup!");
            //friend.GetComponent<NavMeshAgent>().Stop();
            friend.GetComponent<NavEnemyController>().CheckPointBeforeVisit(new Vector3(Random.Range(transform.position.x, transform.position.x+0.4f), transform.position.y, Random.Range(transform.position.x, transform.position.z - 0.4f))); 
            friend.GetComponent<NavEnemyController>().CallMyFriends = true;
        }

    }
    void Update()
    {

       
        //check if player exist 
        if (target)
        {
            timer += Time.deltaTime;


            float distance = Vector3.Distance(target.position, transform.position);


            //check if player is Visible for enemy
            if (distance <= LookRadius)
            {
                dontAllowRandom = true;
                lastSeenPoint = target.position;
                //remeber last seen position for next search
                VisitedPosition = false;

                allowTrigger = true;
                CheckPointBeforeVisit(target.position / AttackDistanceDivider);
                print("found target");
                CallBackup();
                RogerThat = true;
                //check if distance for shoot true
                if (distance <= LookRadius / AttackDistanceDivider)
                {
                    FaceTraget();
                    Attack(true);

                    print("attack!");

                    //if player too close, move forward
                    if (distance <= DeadZone)
                    {
                        print("---Too close, change distance---");
                        
                        CheckPointBeforeVisit(target.position * (AttackDistanceDivider / 2));
                    }
                }
                else
                {
                    Attack(false);
                }
           
            }
            else if (!VisitedPosition)
            {
                //enemy move to last position, where player was seen
                allowTrigger = true;
                CheckPointBeforeVisit(lastSeenPoint);
                print("Go to last seen position!");
                if(agent.remainingDistance< agent.stoppingDistance)
                {
                    VisitedPosition = true;
                    dontAllowRandom = false;
                    print("I arrive on  last seen position!");
                }
                
                
                Attack(false);
                CallMyFriends = false;
            }
            else if (timer > WaitTimer)
            {
                print("Search");

                if (dontAllowRandom)
                {
                    findTarget();
                }
                findTarget();
                timer = 0;

            }
            if (CallMyFriends && !RogerThat)
            {
                CallBackup();
                RogerThat = true;
            }

            #region Debug

            // if (!(agent.remainingDistance > agent.stoppingDistance)) { 
            //         findTarget();
            //     print("search target");
            // }


            //All enemies run to middle click position

            if (Input.GetMouseButtonDown(2))
            {

                //agent.Stop();

            }
            if (Input.GetMouseButtonDown(2)&&Input.GetKey(KeyCode.LeftControl))
            {

               
                //agent.velocity = Vector3.zero;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 100f))
                {
                    print(hit.point);
                    agent.isStopped = false;
                   agent.SetDestination(hit.point);
                }
            }
            /*if (agent.remainingDistance > agent.stoppingDistance)
            {
                //character.Move(agent.desiredVelocity, false, false);

            }
            else
            {
                //character.Move(Vector3.zero, false, false);

            }*/
            #endregion Debug

        }
        else
        {
            //agent.Stop();
            Attack(false);
        }


   
    }
    public void OnDrawGizmos()
    {
        if (target)
        {
            float distance = Vector3.Distance(target.position, transform.position);
            if (CallMyFriends)
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawWireSphere(transform.position, LookRadius);
            }
            else
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireSphere(transform.position, LookRadius);
            }

            if (distance <= LookRadius)
            {

                Gizmos.color = Color.white;
                Gizmos.DrawWireSphere(transform.position, LookRadius / AttackDistanceDivider);
                Gizmos.color = Color.blue;
                Gizmos.DrawWireSphere(transform.position, DeadZone);
            }

        }


    }


}

